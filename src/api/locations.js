export const fuzzySearch = async (query) => {
  const response = await fetch(
    `/locations?q=${encodeURIComponent(query)}`
  )

  const data = await response.json()
  return { response, data }
}