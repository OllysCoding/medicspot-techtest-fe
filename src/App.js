import './App.scss'
import { useFuzzyLocation } from 'hooks/locations.js'

function App() {
  const { results, onInput, error } = useFuzzyLocation()

  return (
    <div className="app">
      <h1>Search</h1>
      <input placeholder="Search ..." onInput={(e) => onInput(e)}></input>
      <h2>Results</h2>
      <div class="app__results">
        {
          results && results.data.length ? results.data.map((result, i) => 
            <p key={i} className="app__result"><span>{result.name}</span> - {result.latitude}, {result.longitude}</p>
          ) : <p>{ error || 'No Results' }</p>
        }
      </div>
    </div>
  )
}

export default App
