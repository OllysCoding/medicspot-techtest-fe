import { fuzzySearch } from 'api/locations'
import { useState } from 'react'

export const useFuzzyLocation = () => {
  const [input, setInput] = useState('')
  const [results, setResults] = useState({})
  const [error, setError] = useState(null)

  const onInput = (e) => {
    setError(null)

    const value = e.target.value
    if (value.length > 2) {
      setInput(value)
      fuzzySearch(value)
        .then((results) => {
          if (!results.response.ok) setError(results.data.message)
          else setResults({ query: value, data: results.data })
        })
        .catch((err) => setError(err.message || 'An unknown error occurred'))
    } else setInput('')
  }


  return { 
    results: results && results.query === input ? results : null, 
    onInput,
    error
  }
}