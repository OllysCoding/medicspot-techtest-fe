# Medicspot Technical Test - FE

Frontend React.js based SPA for Medicspot Technical Test

## Getting Started

1. Clone the repo: `git clone git@gitlab.com:OllysCoding/medicspot-techtest-fe.git`
1. Install packages with `npm install`
1. Run in dev mode with `npm run start`
1. Your browser should open on `localhost:3000`

This SPA interacts with a node.js API requests in dev mode are proxied to `localhost:3001`.

### Building

You can build the application by running `npm run build`

## Rationale

I started by using create-react-app to spin up the react project. create-react-app will typically give you everything you need for most React projects, so then only modification I decided to make as to install `node-sass` so I could write my (albeit incredibly basic) styles in `scss`.

I then quickly set about getting an input field on the page, and hooking into the `onInput` event. Unlike `onChange` this is called whenever the user typed, which allowed the 'live search' type described in the spec. I had already decided to use hooks and functional components for the project, so I first worked on getting all the logic working using `useState` within the component, then created my own hook `useFuzzyLocation` to abstract the logic away, keeping the component easy to understand, and making it easy to re-use the logic if needed down the line (which it will never be, because, y'know, its a tech test, but still, hooks!).

In my implementation I focussed on the line that mentioned it should only display results that are up-to-date with the users input. So I made sure my async code factored this in; storing the query used with the results returned in state so they could be compared with the current input during the render.

I didn't focus heavily on error handling, but did pass back a simple message to the user if the API returned one or there was an error with the request. This error message was just the raw API message though, meaning the API would have to return user-friendly error messages, rather than ones which are useful for developers. This would definitely be something I changed if I spent more time on it.

Overall after this I was relatively happy with the solution, basic testing showed it worked, that there are places with 'test' in their name in the UK, and that API errors from searches like '%%%' would show an error to the user. 
